<?php
	/**
	 * Classe gérant la disposition du code HTML du jeu
	 */
	class Vue_manager
	{

		function __construct()
		{

		}

		//Page de sélection de jeu
		public function select_game_page()
		{
			global $game_engine;

			//Header par défaut
			$this->header();

			//Formulaire
			echo "<div id='middle'>";
			echo "<h3>Choisissez votre jeu :</h3>";
			echo $game_engine->login_manager()->select_game_form_to_string();
			echo "</div>";

			//Pied de page par défaut
			$this->footer();
		}

		//Page de création de l'avatar (connexion)
		public function home_page()
		{
			global $world;
			global $game_engine;

			//Header personnalisé (cf header())
			$this->header();

			//Message de bienvenue
			echo "<div id='middle'>".
				 "<article id='welcome'><h2>Bienvenue !</h2><p>".
				 $world->welcome_message().
				 "</p></article><hr>";

			//Formulaire de création de l'avatar
			echo "<h3>Choisissez votre avatar :</h3>";

			echo $game_engine->login_manager()->login_form_to_string();
			echo "</div>";

			//Pied de page par défaut
			$this->footer();

		}

		//Page de déconnexion
		public function logout_page($value='')
		{
			global $world;
			global $game_engine;

			//header personnalisé
			$this->header();

			//Message de "contraire de bienvenue" (biendépart ?)
			echo "<div id='middle'>".
				 "<article id='good_bye'><h2>A bientôt !</h2><p>".
				 $world->good_bye_message().
				 "</p></article><hr>";

			//Formulaire de choix du jeu
			echo "<h3>Tentez une autre aventure :</h3>";

			echo $game_engine->login_manager()->select_game_form_to_string();
			echo "</div>";
			$this->footer();
		}

		//Page d'erreur personnalisable par indication du message d'erreur
		public function error_page($error)
		{
			global $world;
			global $game_engine;

			//Header
			$this->header();

			//Message d'erreur
			echo "<div id='middle'>".
				 "<article id='error'><h3>ERREUR</h3><p>".
				 $error.
				 "</p><p>Vous avez été déconnecté(e).</article>";

			//Formulaire de choix du jeu
			echo "<h3>Tentez une autre aventure :</h3>";

			echo $game_engine->login_manager()->select_game_form_to_string();
			echo "</div>";

			//Footer
			$this->footer();
		}

		//Page en jeu (la principale)
		public function ingame_page($story, $script)
		{
			global $world;
			global $player;
			global $game_engine;

			//Header
			$this->header();

			//récupération de la pièce courante
			$room = $world->room($player->position());

			//Coeur de la page
			echo '<section id="game">';

			//Préparation du canevas de la carte
			echo '<canvas id="map_canvas", width="400" height="300">'.
				 'Carte du monde</canvas>';

			//Infos du joueur
			//nom
			echo '<div id="info">'.
				 '<h3 id="player_name">'.
				 $player->name(). '</h3>';

			//pièce
			echo '<p id="position">'.
				 $room->name(). '</p><hr>';

			//inventaire
			echo '<div id="inv">';
			echo $player->inventory_to_string();
			echo '</div></div>';

			//Histoire fournie en argument
			echo '<div id="story">'.
				 $story.
				 '</div>';

			//Mouvements possibles
			echo '<div id="moves">';
			echo $room->outs_to_string();
			echo '</div>';

			//Objets à ramasser
			echo '<div id="loot">';
			echo $room->obj_to_string();
			echo '</div>';

			//Bouton (lien enjolivé à grand coup de CSS) de déconnexion
			echo '<p id="logout">';
			echo $game_engine->login_manager()->logout_link();
			echo '</p>';

			echo '</section>';

			//Footer
			$this->footer($script);
		}

		//Affichage du header en fonction de si un jeu est sélectionné ou non
		private function header()
		{
			global $world;
			global $game_engine;

			//Si le jeu est choisi, titre et sous-titre personnalisés
			if($game_engine->login_manager()->game_selected()){
				$title = $world->title();
				$subtitle = $world->subtitle();
			} else {
				//Sinon juste un titre humble et modeste, j'y tiens
				$title = "Best PHP and JS games EVER !";
				$subtitle = "";
			}

			//Utilisation d'une syntaxe bizzare et plus ou moins pratique
			echo <<<EOS
					<!DOCTYPE html>
					<html>
					<head>
						<meta charset="utf-8">
						<link rel="stylesheet" href="CSS/style.php"
							  type="text/css" media="all">
						<title>
EOS;
			//Tire d'onglet = Titre de page
			echo $title;
			echo <<<EOS
					</title>
					</head>

					<body>
						<header id="main_title">
							<h1>
EOS;
			//Titre et sous-titre dans le header
			echo $title . "</h1><h2>";
			echo $subtitle;
			echo "</h2></header>";
		}


		//Affichage du footer en fonction des scripts à charger
		private function footer($script = array())
		{
			//Scripts toujours présents : jsquery et ma toolbox
			echo "<footer><p>Crédit : Mr. BABAFOOOO !</p></footer>".
				 '<script src="js/jquery-2.2.3.js"'.
				 'type="text/javascript" charset="utf-8"></script>'.

				 '<script src="js/toolbox.js"'.
				 'type="text/javascript" charset="utf-8"></script>';

			//Les scripts personnalisés à charger
			foreach ($script as $url)
				echo '<script src="'.$url.
					 '" type="text/javascript" charset="utf-8"></script>';

			echo '</body></html>';

		}
	}

 ?>