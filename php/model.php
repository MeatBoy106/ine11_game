<?php
/*
 * Ce fichier contient tous les éléments de la partie modèles du modèle MVC
 *
 * class World
 * class Room
 * class Player
 * class Inventory
 	* class Condition
 * class Object
 */


/*
 * Représentation du monde
 */
class World
{
	private $m_rooms; //Tableau des pièces du jeu
	private $m_room_nb; //Nombre de pièces
	private $m_objects_list; //Tableau des objets du jeu
	private $m_welcome_message; //Message de bienvenue
	private $m_good_bye_message;	//Message de déconnexion
	private $m_images; //Personnages de jeu
	private $m_title; //Titre du jeu
	private $m_subtitle; //Sous-titre du jeu

	function __construct($json_file)
	{
		# Construire le monde d'origine à partir du champ 'world' du json
		# Et les objets à partir du champ 'objects'
		$all = json_decode(file_get_contents($json_file), true);
		$world = $all['world'];
		$objects = $all['objects'];

		//Les propriétés triviales à remplir
		$this->m_welcome_message = $world['welcome'];
		$this->m_good_bye_message = $world['good_bye'];
		$this->m_images = $all['images'];
		$this->m_title = $all['title'];
		$this->m_subtitle = $all['subtitle'];

		//Construction de la liste des objets
		foreach ($objects as $object)
			$this->m_objects_list[$object['id']] = new Object(
						$object['id'], $object['name'],
						new Condition($object['condition']),
						$object['message_OK'], $object['message_KO']);

		$this->m_room_nb = 0;
		//Construction de la liste des pièces
		foreach ($world['rooms'] as  $room) {
			$this->m_room_nb ++;
			$this->m_rooms[$room['id']] = new Room(
						$room['id'], $room['name'],
						$room['position'], $room['color'],
						$room['outs'], $room['obj'],
						$room['end'], $room['script']);
		}

		//La prise en compte des objets déjà ramassés
		//est faite par la pièce concernée.
	}

	//Les accesseurs
	public function room($id) { return $this->m_rooms[$id]; }
	public function room_nb() { return $this->m_room_nb; }
	public function object($id) { return  $this->m_objects_list[$id]; }
	public function welcome_message() { return $this->m_welcome_message; }
	public function good_bye_message() { return $this->m_good_bye_message; }
	public function images() { return $this->m_images; }
	public function title() { return $this->m_title; }
	public function subtitle() { return $this->m_subtitle; }
}


/*
* Représentation d'une pièce
*/
class Room
{
	private $m_id; //id de la pièce
	private $m_name; //Nom de la pièce
	private $m_color; //Couleur de la pièce (puis image ?)
	private $m_geometry; //Pour l'affichage sur le canevas
	private $m_outs; //Tab sorties (noms, conditions, message_OK, message_KO)
	private $m_obj; //Contenu original de la pièce
	private $m_taken_obj; //Objets pris dans la pièce
	private $m_end; //Si la pièce contient une fin de jeu
	private $m_script; //Scripts personnalisés

	function __construct($id, $name, $position, $color, $outs, $obj, $end, $script)
	{
		//Propriétés triviales
		$this->m_id = $id;
		$this->m_name = $name;
		$this->m_color = $color;
		$this->m_geometry = $position;
		$this->m_end = $end;
		$this->m_script = $script;
		$this->m_obj = new Inventory($obj);

		//Les objets déjà ramassés dans la pièce
		if(isset($_SESSION['taken_obj'][$this->m_id]))
			$this->m_taken_obj = $_SESSION['taken_obj'][$this->m_id];
		else
			$this->m_taken_obj = new Inventory();

		//On désencapsule et réorganise les sorties
		foreach ($outs as $out)
			$this->m_outs[$out['dest']] = array(
						'by' => $out['by'],
						'condition' => new Condition($out['condition']),
						'message_OK' => $out['message_OK'],
						'message_KO' => $out['message_KO']);

	}

	function __destruct()
	{
		//Sauvegarde de ce qui doit l'être, à savoir les objets ramassés
		$_SESSION['taken_obj'][$this->m_id] = $this->m_taken_obj;
	}

	//Les accesseurs
	public function id() { return $this->m_id; }
	public function name() { return $this->m_name; }
	public function color() { return $this->m_color; }
	public function x() { return $this->m_geometry[0]; }
	public function y() { return $this->m_geometry[1]; }
	public function w() { return $this->m_geometry[2]; }
	public function h() { return $this->m_geometry[3]; }
	public function end_val() { return $this->m_end['val']; }
	public function end_text() { return $this->m_end['text']; }
	public function script(){ return $this->m_script; }

	public function condition($out)
	{
		//Retourne la condition pour rejoindre la sortie $out
		if(isset($this->m_outs[$out]))
			return $this->m_outs[$out]['condition'];
		else return NULL;
	}

	public function message_OK($out)
	{
		//Retourne le message de refus de rejoindre la sortie $out
		if(isset($this->m_outs[$out]['message_OK']))
			return $this->m_outs[$out]['message_OK'];
		else return "Texte manquant";
	}

	public function message_KO($out)
	{
		//Retourne le message de refus de rejoindre la sortie $out
		if(isset($this->m_outs[$out]['message_KO']))
			return $this->m_outs[$out]['message_KO'];
		else return "Texte manquant";
	}

	//Retire un inventaire à la pièce sans vérification donc négatifs possibles
	//Utilisé avec des singletons mais on peut imaginer plus général
	public function remove_inv($inventory)
	{
		//Retirer un inventaire revient à l'ajouter à l'inventaire
		//des objets ramassés
		$this->m_taken_obj =
			$this->m_taken_obj->add_inv($inventory);
	}

	//Création de la liste des mouvements possibles
	public function outs_to_string()
	{
		global $world;

		$string = "<p>Mouvement(s) possible(s) :</p><ul>";
    	foreach ($this->m_outs as $destination => $details) {
            $string .= "<a href='index.php?action=go&to=" . $destination . "'><li>";
            $string .= "Rejoindre '" . $world->room($destination)->name() . "'";
            $string .= " en passant par '" . $details['by'] . "'.</li></a>";
        }
        $string .= "</ul>";

        return $string;
	}

	//Création de la liste des objets ramassables
	public function obj_to_string()
	{
        $string = "<p>Objet(s) ramassable(s) :</p>";
        //Utilisation de la méthode to_string sur l'inventaire des objets restants
		$string .= $this->m_obj->sub_inv($this->m_taken_obj)->to_string(true);

		return $string;
	}

	//
	public function end_to_string()
	{
		//Si ce n'est pas un pièce de fin potentielle, chaine vide
		if($this->m_end[0] == 'none' )
			$string = "";
		else {
			//Sinon, div contenant victoire ou défaite et un texte explicatif
			$string = "<div id='end_text'>";
			if($this->end_val() == 'win')
				$string .= "<h3>VICTOIRE !</h3>";
			elseif($this->end_val() == 'lose')
				$string .= "<h3>DEFAITE !</h3>";

			$string .= "<p>" . $this->end_text() . "</p></div>";
		}

		return $string;
	}
}

/*
 * Représentation du joueur
 */
class Player
{
	private $m_name; //Nom du joueur
	private $m_image; //Image du joueur
	private $m_position; //Pièce où se trouve le joueur
	private $m_inventory; //Inventaire du joueur

	function __construct()
	{
		$this->m_name = $_SESSION['username'];
		$this->m_image = $_SESSION['image'];
		$this->m_position = $_SESSION['position'];
		$this->m_inventory = $_SESSION['inventory'];
	}

	function __destruct()
	{
		//Sauvegarde de la position et de l'inventaire
		$_SESSION['position'] = $this->m_position;
		$_SESSION['inventory'] = $this->m_inventory;
	}

	//Accesseurs
	public function name() { return $this->m_name; }
	public function image()	{ return $this->m_image; }
	public function position() { return $this->m_position; }
	public function inventory() { return $this->m_inventory; }

	//Ramasser un inventaire
	public function take($inventory)
	{
		$this->m_inventory =
			$this->m_inventory->add_inv($inventory);
	}

	//Payer une condition sans vérifier que c'est possible
	public function pay($condition)
	{
		$this->m_inventory =
			$condition->apply_on_inventory($this->m_inventory);
	}

	//Déplacer le joueur sans vérifiacation que c'est possible
	public function move($to) {	$this->m_position = $to; }

	//Renvoie la chaine HTML affichant l'inventaire
	public function inventory_to_string()
	{
		$string = "<p>Inventaire :</p>";
		$string .= $this->m_inventory->to_string(false);

		return $string;
	}
}


/*
* Représentation d'un inventaire
*/
class Inventory
{
	protected $m_objects; //Objets composant l'inventaire

	function __construct()
    {
    	//Ruse de sioux pas de moi pour surcharger le constructeur
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
    }

    //L'inventaire vide
    function __construct0()
    {
		$this->m_objects = array();
    }

    //L'inventaire général (multi-objets)
	function __construct1($objects)
	{
		$this->m_objects = array();
		foreach ($objects as $detail)
			$this->m_objects[$detail['id']] = $detail['quantity'];
	}

	//Inventaire avec un seul objet
	//(pratique de pouvoir le créer comme ça dans certains cas)
	function __construct2($id, $quantity)
	{
		$this->m_objects[$id] = $quantity;
	}

	//Accesseur
	public function objects() { return $this->m_objects; }

	//Quantité stockée d'un objet donné par $id
	public function quantity($id)
	{
		return isset($this->m_objects[$id]) ? $this->m_objects[$id] : 0;
	}

	//Ajout d'un objet $id en quantité $quantity sur l'inventaire lui-même
	public function add_obj($id, $quantity)
	{
		//Ajoute un objet sur l'inventaire lui-même
		if($this->quantity($id) === 0)
			$this->m_objects[$id] = $quantity;
		else
			$this->m_objects[$id] += $quantity;
	}

	//Ajout d'un inventaire $inventory sur une copie
	//retour de la copie modifiée
	public function add_inv($inventory)
	{
		//Ajoute un inventaire à celui-ci et renvoie une copie

		$new_inventory = clone $this;

		foreach ($inventory->objects() as $id => $quantity)
			$new_inventory->add_obj($id, $quantity);

		return $new_inventory;
	}

	//Soustration d'un inventaire $inventory sur une copie
	//retour de la copie modifiée
	public function sub_inv($inventory)
	{
		//Valeurs négatives possibles
		$new_inventory = clone $this;

		//On ajoute objet par object le 1er inventaire
		foreach ($inventory->objects() as $id => $quantity)
			$new_inventory->add_obj($id, -$quantity);

		return $new_inventory;
	}

	//Retourne la liste sans décoration des objets dans l'inventaire
	//Le paramètre $takable indique si les objets doivent êtres des liens
	//pour pouvoir les ramasser
	public function to_string($takable)
	{
		global $world;

		$string = "<ul>";
		foreach ($this->objects() as $id => $quantity)
			if($quantity > 0)
				if($takable)
					$string .= "<a href='index.php?action=take&obj=".$id."'><li>".
					$world->object($id)->name() . " : " . $quantity ."</li></a>";
				else
					$string .= "<li>" . $world->object($id)->name() . " : "
						 . $quantity ."</li>";

		$string .= "</ul>";

		return $string;
	}
}

/*
* Décrit une condition (Inventaire avec critère indiquant si un objet est consommé
* lors de l'application de la condition)
*/
class Condition extends Inventory
{
	private $m_consumed;

	function __construct($objects)
	{
		//Constructeur parent
		parent::__construct($objects);
		//Ajout de l'information de "consommabilité" de l'objet
		foreach ($objects as $detail)
			$this->m_consumed[$detail['id']] = $detail['consumed'];
	}

	//Retourne si l'objet $id est consommés lors de l'application de la condition
	private function consumed($id) {
		//Dans le cas où 'consumed' n'est pas précisé, on considère
		//que l'objet n'est pas consommé.

		return isset($this->m_consumed[$id]) ? $this->m_consumed[$id] : false;
	}

	//Retourne la compatibilité d'un inventaire avec un condition.
	public function test_inventory($inventory)
	{
		//L'inventaire NULL ne remplit aucune condition
		if ($inventory === NULL) return false;

		foreach ($this->objects() as $id => $detail){
			if($this->quantity($id) > $inventory->quantity($id))
				return false;
		}
		return true;
	}

	//Applique la condition sur une copie de l'inventaire $inventory
	//$forced_consumption indique si on doit consommer tous les objets
	//indépendament de s'il sont sensés l'être ou non.
	//C'est utile plus tard et transparent grace à la valeur par défaut
	public function apply_on_inventory($inventory, $forced_consumption = false)
	{
		//Répercution brutale de la condition sans test
		//(valeurs négatives possibles)

		$inv_cpy = clone $inventory;

		//Pour chaque objet de la condition, s'il est consommé,
		//on le retire de la copie
		foreach ($this->objects() as $id => $detail)
			if ($this->consumed($id) || $forced_consumption)
					$inv_cpy->add_obj($id, -$this->quantity($id));

		return $inv_cpy;
	}

	//Retourne l'inventaire des objets manquants dans l'inventaire
	//pour remplire la condition
	public function lacking_objects($inventory)
	{
		//Nombre relatif d'objets restants après application de la condition
		//avec consommation forcée
		$remaining = $this->apply_on_inventory($inventory, true)->objects();

		//Les objets en nombre négatifs sont ceux qui manqnent
		foreach ($remaining as $id => $lack)
			$remaining[$id] = array('id'=> $id, 'quantity'=> max(0, -$lack));

		var_dump(new Inventory($remaining));
		return new Inventory($remaining);
	}

	//Retourne la chaine HTML affichant les objets manquants au remplissage
	//de la condition par l'inventaire $inventory
	public function lacking_to_string($inventory)
	{
		global $world;

		//Objets manquants
		$lacking = $this->lacking_objects($inventory)->objects();

		$string = "<div id='lacking_obj'><p>Objet(s) manquant(s) :</p><ul>";
		foreach ($lacking as $id => $lack)
			// L'application de la condition avec consommation forcée
			// permet de déduire les objets manquants par la condition suivante
			if($lack > 0)
				$string.= "<li>" . $world->object($id)->name() .
					 "(s) : " . $lack . "</li>";
		$string .= "</ul></div>";

		return $string;
	}
}


/*
* Représentation d'un objet
*/
class Object
{
	private $m_id; //Identifiant de l'objet
	private $m_name; //Nom de l'objet
	private $m_condition; //Condition de ramassage
	private $m_message_OK; //Message acompagnant le ramassage
	private $m_message_KO; //Message d'interdicition de ramassage

	function __construct($id, $name, $condition, $message_OK, $message_KO)
	{
		$this->m_id = $id;
		$this->m_name = $name;
		$this->m_condition = $condition;
		$this->m_message_OK = $message_OK;
		$this->m_message_KO = $message_KO;
	}

	public function condition() { return $this->m_condition; }
	public function message_OK() { return $this->m_message_OK; }
	public function message_KO() { return $this->m_message_KO; }
	public function name() { return $this->m_name; }
	public function id() { return $this->m_id; }
}
?>