<?php
	/**
	 * Ce fichier contienttous les éléments de la partie controleur du modèle MVC
	 * Login_manager
	 * Game_engine
	 */


	/**
	* Gère le fonction du jeu à proprement parler (tests et conséquences des actions)
	*/
	class Game_engine
	{
		private $m_login_manager; //Gestionnaire ed connexion (et actions liées)
		private $m_game_interface; //Gestionnaire de communication (et actions liées)

		function __construct()
		{
			$this->m_login_manager = new Login_manager();
			$this->m_game_interface = new Game_interface();

			//Initialisation du modèle si utilisateur connecté et si l'action
			//demandée le nécessite
			if($this->m_login_manager->loged_in())
				if(isset($_GET['action']) &&
				   $_GET['action'] != 'read_var' &&
				   $_GET['action'] != 'set_var'){
						$this->init();
				}
		}

		//Accesseur
		public function login_manager() { return $this->m_login_manager; }

		//Gestion de l'action demandée
		public function handler()
		{
			//Le README offre les détails de l'utilisation de $_GET
			if (! isset($_GET['action']))
				//Page d'accueil par défaut
				$this->m_login_manager->select_game_action();
			else {
				$action = $_GET['action'];
				//Actions possibles hors connexion
				switch ($action) { 
					case 'set_var':
						$this->m_game_interface->set_var_action();
						return;
						break;
					case 'read_var':
						$this->m_game_interface->read_var_action();
						return;
						break;
				}
				//Actions possibles seulement si connecté
				if($this->m_login_manager->loged_in()){
						switch ($action) {
							case 'logout':
								$this->m_login_manager->logout_action();
								break;
							case 'go':
								$this->go_action();
								break;
							case 'take':
								$this->take_action();
								break;
							case 'map':
								$this->m_game_interface->map_action();
								break;
							case 'where':
								$this->m_game_interface->where_action();
								break;
							default:
								$this->unknown_action();
								break;
						}
				}
				//Actions possibles seulement si non connecté
				else switch ($action) {
					case 'home':
						$this->m_login_manager->home_action();
						break;
					case 'login':
						$this->m_login_manager->login_action();
						break;
					default:
						$this->unknown_action();
						break;
				}
			}
		}

		//Initialisation du modèle
		public function init()
		{
			global $world;
			global $player;

			if($this->m_login_manager->game_selected()){
				//Initialisation réalisée avant la connexion pour démarrer
				//avec les bonnes caractéristiques de joueur
				if(! $this->m_login_manager->loged_in()){
					$_SESSION['position'] = 1;
					$_SESSION['inventory'] = new Inventory();
					$_SESSION['taken_obj'] = array();
					$_SESSION['image'] = "nobody.png";
				}

				//Création des modèles du joueur et du monde à partir du json
				$player = new Player();
				$world = new World($_SESSION['game']);
			}
		}

		public function go_action()
		{
			//Fonction appelée dans le cas d'une action 'go'
			global $world;
			global $player;
			global $vue_manager;

			//Si erreur d'URL
			if(! isset($_GET['to'])){
				$this->unknown_action();
				return;
			}

			//Pièce de départ (from) et d'arrivée (to)
			$from = $world->room($player->position()); //Objet réel
			$to = $_GET['to']; //Indice de la pièce seulement

			//Vérification des conditions et prépapration
			//de l'affichage en conséquence
			$condition = $from->condition($to);
			if($condition === NULL){ //Déplacement interdit (pas prévu dans le jeu)
				$this->unknown_action();
				return;
			}

			$inventory = $player->inventory();
			$story = "";
			$script = array();

			//Si condition de déplacment replie
			if($condition->test_inventory($inventory)){
				//On répercute la condition sur l'inventaire
				$player->pay($condition);
				//On effectue le changement de pièce
			   	$player->move($to);

			   	//Ecriture de l'histoire
			   	$story = '<p>' . $from->message_OK($to) . '</p>';
			   	$story .= $world->room($to)->end_to_string();

			   	//Récupération des scripts dédiés à la page
			   	$script = $world->room($to)->script();
			} else {

				//Ecriture de l'histoire
			   	$story = '<p>' . $from->message_KO($to) . '</p>';
			   	$story .= $world->room($to)->end_to_string();
			   	$story .= $condition->lacking_to_string($inventory);

				$script = $from->script();
				//On ne change pas de pièce
			}

			//Appel de la vu sur une page en jeu
			$vue_manager->ingame_page($story, $script);
		}

		public function take_action()
		{
			//Fonction appelée dans le cas d'une action 'take'
			global $world;
			global $player;
			global $vue_manager;

			//Si erreur d'URL
			if(! isset($_GET['obj'])){
				$this->unknown_action();
				return;
			}

			$obj = $world->object($_GET['obj']);
			if($obj === NULL){ //Objet inexistant
				$this->unknown_action();
				return;
			}

			//Vérification de la condition
			$condition = $obj->condition();
			$inventory = $player->inventory();
			$room = $world->room($player->position());

			if($condition->test_inventory($inventory)){
				//On répercute la condition sur l'inventaire et la pièce
				$player->pay($condition);
				$taken_inventory = new Inventory($obj->id(), 1);
				//On retire l'objet dans la pièce
				$room->remove_inv($taken_inventory);
				//On effectue le ramassage
			   	$player->take($taken_inventory);

			   	$story = '<p>' . $obj->message_OK($to) . '</p>';
			} else {
				$story = '<p>' . $obj->message_KO($to) . '</p><hr>' .
						  $condition->lacking_to_string($inventory);
				//On ne ramasse pas l'objet
			}
			$script = $room->script();

			$vue_manager->ingame_page($story, $script);
		}

		//Appelée en cas d'action inconnue ou invalide
		private function unknown_action()
		{
			global $vue_manager;

			//Déconnexion
			$this->m_login_manager->logout();

			//Appel de la vue sur une page d'erreur
			$vue_manager->error_page("Action inconnue");
		}
	}


	/**
	* Gère la connexion, la récupération des informations et la déconnexion
	*/
	class Login_manager
	{
		function __construct()
		{
			//Initialisation de la session
			//Cet emplacement garanti que la session n'est pas
			//utilisée avant
			session_start();
		}

		//Accesseur
		public function loged_in() { return isset($_SESSION['username']); }
		public function game_selected() { return isset($_SESSION['game']); }
		
		//Appelée en cas d'action de connexion (login)
		public function login_action()
		{
			global $vue_manager;
			global $game_engine;
			global $world;
			global $player;

			//Vérification des paramètres GET
			if(isset($_POST['username']) && isset($_POST['image'])){
				$_SESSION['username'] = $_POST['username'];
				$_SESSION['image'] = $_POST['image'];
			} else {
				$game_engine->unknown_action();
			}

			//Initialisation car pas faite lors de la construction de $game_engine
			//L'utilisateur n'était pas connectée et les données non disponibles
			$game_engine->init();

			//Récupération des scripts dédiés et affichage en jeu sur la première salle
			$script = $world->room($player->position())->script();
			$vue_manager->ingame_page("L'aventure commence !", $script);
		}

		//Appelée en cas d'action de déconnexion (lopgout)
		public function logout_action()
		{
			global $vue_manager;

			//Déconnexion
			$this->logout();

			//Appel de la vue sur la page de déconnexion
			$vue_manager->logout_page();
		}

		//Affichage du lien de déconnexion
		public function logout_link()
		{
			return '<a href="index.php?action=logout">Déconnexion</a>';
		}


		//Retourne la chaine correspondant au formulaire de connexion
		public function login_form_to_string()
		{
			global $world;
			$images = $world->images();

			$string = "<form id='login_form' action='index.php?action=login'".
					 "method='post'>";

			$string.= "<label for='username'>Pseudonyme : &nbsp;</label>";
		    $string.= "<input type='text' name='username' id='username'".
		    		  "placeholder='Mon pseudo'><br>";

			$string.= "<label for='image'>Avatar : &nbsp;</label>";
		    $string.= "<select name='image' id='image'>";
		    //Liste des avatars disponibles
			foreach ($images as $name => $path)
				$string.= "<option value='".$path."'>" . $name . "</option>";
		    $string .= "</select>";
		    
		    $string.= "<input type='submit' value='Connexion'>" ;

		    $string.= "</form>";

		    return $string;
		}

		//Retourne la chaine correspondant au formulaire de sélection du jeu
		public function select_game_form_to_string()
		{
			$games = scandir("games");

			$string = "<form id='login_form' action='index.php?action=home'".
					  "method='post'>";

			$string.= "<label for='select_game'>Fichier de jeu : &nbsp;</label>";
		    $string.= "<select name='game' id='select_game'>";
			//Liste des jeux disponibles (nom du fichier json sans l'extension)
			foreach ($games as $json){
				$path = 'games/' . $json;
				if(! is_dir($path))
					$string .= "<option value='".$path."'>" .
							   explode('.', $json)[0] . "</option>";
			}
		    $string .= "</select>";

		    $string .= "<input type='submit' value='Entrer'>" ;

		    $string .= "</form>";

		    return $string;
		}

		//Appelée lors d'une action home (page de connexion)
		public function home_action()
		{
			global $vue_manager;
			global $game_engine;

			if(isset($_POST['game'])){
				$_SESSION['game'] = $_POST['game'];
				//Initialisation requise pour récupérer le thème d'affichage
				$game_engine->init();
				//Appel de la vue sur la page de connexion (home)
				$vue_manager->home_page();
			} else
				//Affichage de la page de choix de jeu
				$this->select_game_action();
		}

		//Appelée lors de l'abscence d'action (action par défaut)
		public function select_game_action()
		{
			global $vue_manager;

			//Affichage de la page de sélection du jeu
			$vue_manager->select_game_page();
		}

		//Procédure de déconnexion
		public function logout()
		{
			//Réinitialisation de la session
			session_destroy();
		}
	}



	/**
	* Interface du jeu avec l'extérieur :
	 * réponse aux requêtes de carte et de position du joueur
	 * Lecture et écriture de variables en SESSION pour d'éventuels scripts
	 * extension avec fonction de sauvegarde, etc. possible.
	*/
	class Game_interface
	{
		//Retourne un json des éléments visuels du monde (géométrie et couleurs)
		private function world_to_json()
		{
			/**
			 * Cette fonction ne sert qu'à répondre à la question demandant
			 * de faire un JSON. json_encode utilisée plus bas fait mieux
			 * le travail en temps normal.
			 * J'ai pris des libertés sur la forme demandée...
			 */
			global $world;
		    $json = "[";
		    for ($i=1; $i <= $world->room_nb() ; $i++) {
		    	$room = $world->room($i);
		    	$color = $room->color();
		    	$json .= "{\"color\": \"" . $color . "\", \"geometry\": {";
		        $json .= '"x":'. $room->x() . ", ".
		        		 '"y":'. $room->y() . ", ".
		        		 '"w":'. $room->w() . ", ".
		        		 '"h":'. $room->h() . "}}, ";

		    }
		    $json = trim($json, ", ");
		    $json .= "]";
		    return $json;
		}

		//Appelée lors d'un action map (requête ajax)
		public function map_action()
		{
			//Le header qui va bien...
			header("Content-Type: application/json; charset=UTF-8");
    		//... et la chaine qui va bien
    		echo $this->world_to_json();
		}

		//Appelée lors d'une action where (requête ajax)
		public function where_action()
		{
			//Le header qui va bien
			header("Content-Type: application/json; charset=UTF-8");

			global $world;
			global $player;

			//Récupération des infos de position du centre de la pièce courante
		    $room = $world->room($player->position());
		    $center['x'] = $room->x() + 0.5 * $room->w();
		    $center['y'] = $room->y() + 0.5 * $room->h();

		    //Mise en tableau pour json_encode
		    $data = array('image' => $player->image(),
		    			  'position' => array($center['x'], $center['y']));

		    //Affichage
		    echo json_encode($data);
		}

		//Mise en session d'une variable fournie par un script JS
		public function set_var_action()
		{
			//Vérifiaction de l'URL
			if (isset($_GET['name']) && isset($_GET['value'])) {
				//Ajout d'un préfix au nom demandé pour empêcher d'interférer
				//avec les varaibles utilisées côté PHP
				$name = 'u_'. $_GET['name'];
				$value = $_GET['value'];

				//Enregistrement
				$_SESSION[$name] = $value;
			}
		}

		//Envoie de la valeur d'une variable demandée par un script JS
		public function read_var_action()
		{
			//On commence par le header
			header("Content-Type: application/json; charset=UTF-8");

			//Vérification de l'URL
			if(isset($_GET['name']) && isset($_SESSION['u_'.$_GET['name']])){
				//Le header
				//Ajout du préfixe au nom renseigné
				$name = 'u_'. $_GET['name'];
				echo json_encode($_SESSION[$name]);
			} else {
				//O valeur par défaut. Arbitraire mais pratique pour éviter
				//de lourdes vérifications d'existence côté JS
				echo json_encode(0);
			}
		}
	}
?>