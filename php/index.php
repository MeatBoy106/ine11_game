<?php
include_once('php/model.php');
include_once('php/controler.php');
include_once('php/vue.php');

//variables globales
$world; //Modèle du monde
$player; //Modèle du joueur

$game_engine = new Game_engine(); //Moteur de jeu (contrôleur principal)
$vue_manager = new Vue_manager(); //Moteur de rendu (Vue)

//Appel de la fonction de gestion d'action
$game_engine->handler();
?>