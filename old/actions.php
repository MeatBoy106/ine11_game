<?php

function main(){
    if (isset($_SESSION["username"])) {

        if(isset($_GET["action"])) {
            if($_GET["action"] === "map")
                map_action();
            elseif($_GET["action"] === "where"){
                where_action();
            } else {
                common_header();
                switch($_GET["action"]) {
                case "logout":
                    logout_action();
                    break;
                case "go":
                    go_action();
                    break;
                case "take":
                    take_action();
                    break;
                default:
                    unknown_action();
                    break;
                }
                common_footer("script.js");
            }
        } else {
            default_action();
            common_footer("script.js");
        }

    } elseif (isset($_GET["action"]) && $_GET["action"] === "login"){
        login_action();

    } else {
        common_header();
        show_login_form_action();
        common_footer("script.js");
    }

}

function logout_action(){
    echo "<p>Adieu " . $_SESSION["username"] . "</p>";
    session_destroy();
    show_login_form_action();
}

function login_action(){
    $_SESSION["username"] = $_GET["username"];
    init_game();
    default_action();
}

function unknown_action(){
    echo "<p>ERROR : unknown_action</p>";
    logout_link();
}

function default_action(){
    common_header();

    echo "<p>" . $_SESSION["username"] . ", vous êtes ici : <b>" . get_room()["name"] . ".</b></p>";

    echo "<canvas id='map_canvas', width='400' height='300'>Carte du monde</canvas>";

    if(isset(get_room()["win"])){
        echo "<p><b>VICTOIRE</b></p>";
        echo "<p>" . get_room()["win"] . "</p>";

    } elseif(isset(get_room()["lose"])){
        echo "<p><b>DEFAITE</b></p>";
        echo "<p>" . get_room()["lose"] . "</p>";
    }

    echo "<br>";
    display_movements_list();
    echo "<br>";
    display_objects_list();
    echo "<br>";
    display_inventory();

    echo "<hr/>";
    logout_link();
    map_json_link();
}

///A REVOIR
function go_action(){
    global $world;
    if(isset($_GET["new_room"]) && isset($world[$_GET["new_room"]])){
        $from = $_SESSION["room"];
        $to = $_GET["new_room"];
        $condition = $world[$from]["outs"][$to]["condition"];

        if($condition === 0 || $_SESSION["inventory"][$condition] > 0)
            $_SESSION["room"] = $to;

        else display_lacking_objects($world[$from]["outs"][$to]);

        default_action();
    } else unknown_action();
}

function take_action(){
    global $objects;
    if(isset($_GET["taken_object"])){
        $tk_obj = $_GET["taken_object"];
        $condition = $objects[$tk_obj]["condition"];

        if($condition === 0 || $_SESSION["inventory"][$condition] > 0){
            $_SESSION["inventory"][$tk_obj] ++;
            $_SESSION["taken_objects"][$tk_obj][$_SESSION["room"]] += 1;

        } else display_lacking_objects($objects[$tk_obj]);

        default_action();
    } else unknown_action();
}

function display_lacking_objects($condition){
    global $objects;
    echo "<p>" . $condition["message_KO"] . "<br>Objet à récupérer : <b>" . $objects[$condition["condition"]]["name"] . "</b><p>";
}

function display_movements_list(){
    global $world;
    if(isset($_SESSION["room"])){
        echo "<p>Mouvement possibles :<br><ul>";
        foreach (get_room()["outs"] as $key => $value) {
            echo "<a href='game.php?action=go&new_room=" . $key . "'><li>";
            echo "<p>Rejoindre '" . $world[$key]["name"] . "'";
            echo " en passant par '" . $value["by"] . "'</p></li></a>";
        }
        echo "</ul></p>";
    } else echo "<p><b>ERROR ! No current room</b></p>";
}

function display_objects_list(){
    global $objects;
    if(isset($_SESSION["room"])){
        echo "<p>Objets à ramasser :<br><ul>";
        foreach (get_room()["stuff"] as $key => $value) {
            $remaining = $value - $_SESSION["taken_objects"][$key][$_SESSION["room"]];
            if($remaining > 0){
                echo "<a href='game.php?action=take&taken_object=" . $key . "'><li>";
                echo "<p>". $objects[$key]["name"] . " : " . $remaining . "</p></li></a>";
            }
        }
        echo "</ul></p>";

    } else echo "<p><b>ERROR ! No current room</b></p>";
}

function display_inventory(){
    global $objects;
    if(isset($_SESSION["inventory"])){
        echo "<p>Inventaire :<br><ul>";
        foreach ($objects as $key => $value){
            $nb_obj = isset($_SESSION["inventory"][$key]) ? $_SESSION["inventory"][$key] : 0;
            if($nb_obj > 0) echo "<li>" . $value["name"] . " : " . $nb_obj . "</li>";
        }
        echo "</ul></p>";
    }
}

function get_room(){
    global $world;
    return $world[$_SESSION["room"]];
}

function show_login_form_action(){
    echo "<form action='game.php?action=login' method='GET'>";
    echo "<input type='text' name='username'>";
    echo "<input type='hidden' name='action' value='login'>";
    echo "<input type='submit' value='login'>" ;
    echo "</form>";
}

function map_action()
{
    header("Content-Type: application/json; charset=UTF-8");
    echo world_to_json();
}

function world_to_json(){
    global $world;
    $json_string = "[";
    foreach ($world as $value) {
        $color = $value["color"];
        $position = $value["position"];
        $json_string .= "{\"color\": \"" . $color . "\", \"points\": [";

        foreach ($position as $value_p)
            $json_string .= "[" . $value_p["x"] . ", " . $value_p["y"] . "], ";

        $json_string = trim($json_string, ", ");
        $json_string .= "]}, " ;
    }
    $json_string = trim($json_string, ", ");
    $json_string .= "]";

    return $json_string;
}

function where_action(){
    global $world;
    $room_pos = $world[$_SESSION["room"]]["position"];
    $center = array('x' => 0.5 * ($room_pos[1]['x'] + $room_pos[4]['x']),
                    'y' => 0.5 * ($room_pos[1]['y'] + $room_pos[2]['y']));

    header("Content-Type: application/json; charset=UTF-8");
    echo "[" . $center['x'] . ", " . $center['y'] . "]";
}
?>