$(document).ready(function(){
	$.getJSON( "game.php?action=map", function(map) {
		var canvas = document.getElementById('map_canvas');
		var ctx = canvas.getContext('2d');

		ctx.fillStyle = "rgb(200, 200, 200)";
		ctx.fillRect(0, 0, 400, 300);

		function x(room){ return room.points[0][0]; }

		function y(room){ return room.points[0][1]; }

		function w(room){ return room.points[3][0] - room.points[0][0]; }

		function h(room){ return room.points[1][1] - room.points[0][1]; }

		$.each(map, function(index, room){
			ctx.fillStyle = room.color;
			ctx.lineWidth = 3;
			ctx.fillRect(x(room), y(room), w(room), h(room));
			ctx.strokeRect(x(room), y(room), w(room), h(room));
		});
	});

	$.getJSON("game.php?action=where", function(pos) {
		var canvas = document.getElementById('map_canvas');
		var ctx = canvas.getContext('2d');
		var img = new Image();

		img.onload = function(){
			ctx.drawImage(img, pos[0] - img.width / 2, pos[1] - img.height / 2, img.width, img.height);
		};
		img.src = '../images/bart.png';
	});
});