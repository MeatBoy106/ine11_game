<?php
include "common.php";
include "actions.php";

session_start();

/*
	(x, y)
		___ ___
       |2  |5  |
	___|__D|\__|___
-->|1  |3  |6 \|7  |
   |__>|__+|___|__W|
	   |4  |/
	   |__||

Ordre des coins d'une pièce : 1 4
							  2 3
*/
$world = array(
	1 => array("name" => "Entrée",
		       "outs" => array(3 => array("condition" => 1, "by" => "Tourniquet",
		       										 "message_KO" => "Vous avez besoin d'un billet pour
		       										  				  entrer dans Krustyland.")),
		       "stuff" => array(1 => 1),
		       "color" => "yellow",
		       "position" => room_coordinates(0, 1)),

	2 => array("name" => "Broyeur à ordures",
			   "lose" => "T'es tombé dans le broyeur à ordures, ordure !",
			   "outs" => array(3 => array("condition" => 0, "by" => "Porte enflammée à contre sens")),
		       "stuff" => array(3 => 1, 4 => 3),
		       "color" => "red",
		       "position" => room_coordinates(1, 0)),

	3 => array("name" => "Grand Carrefour",
		       "outs" => array(6 => array("condition" => 2, "by" => "Toboggan",
		       							  "message_KO" => "Vous êtes perdu sans le plan de Krustyland !"),
		       				   4 => array("condition" => 2, "by" => "Chemin",
		       							  "message_KO" => "Vous êtes perdu sans le plan de Krustyland !"),
		       				   2 => array("condition" => 2, "by" => "Porte enflammée",
		       							  "message_KO" => "Vous êtes perdu sans le plan de Krustyland !")),
		       "stuff" => array(2 => 1),
		       "color" => "grey",
		       "position" => room_coordinates(1, 1)),

	4 => array("name" => "Grande Roue",
			   "outs" => array(3 => array("condition" => 0, "by" => "Chemin"),
			   				   6 => array("condition" => 3, "by" => "Tunnel",
			   				   			  "message_KO" => "Le tunnel est bouché ! Trouvez une pelle pour le 			 dégager.")),
		       "stuff" => array(5 => 2),
		       "color" => "grey",
		       "position" => room_coordinates(1, 2)),

	5 => array("name" => "Montagnes Russes",
			   "outs" => array(6 => array("condition" => 0, "by" => "Petit train"),
			   				   2 => array("condition" => 0, "by" => "Toboggan")),
		       "color" => "grey",
		       "position" => room_coordinates(2, 0)),

	6 => array("name" => "Boutiques",
		       "outs" => array(5 => array("condition" => 0, "by" => "Petit train"),
		       				   4 => array("condition" => 3, "by" => "Tunnel",
			   				   			  "message_KO" => "Le tunnel est bouché ! Trouvez une pelle pour le 			 dégager."),
		       				   7 => array("condition" => 4, "by" => "Parcours d'obstacle",
		       				   			  "message_KO" => "Votre voiture a été volée. Trouvez des clefs pour en voler une autre. Ce n'est que justice après tout !")),
		       "stuff" => array(6 => 1, 7 => 1),
		       "color" => "grey",
		       "position" => room_coordinates(2, 1)),

	7 => array("name" => "Parking",
			   "outs" => array(6 => array("condition" => 0, "by" => "Parcours d'obstacles à l'envers")),
			   "win" => "Vous êtes sorti de Krustyland en vie !",
		       "color" => "green",
		       "position" => room_coordinates(3, 1)));

function room_coordinates($x, $y){
	$cx = 100 * $x;
	$cy = 100 * $y;
	return array(1 => array("x" => $cx, "y" => $cy),
				 2 => array("x" => $cx, "y" => $cy + 100),
				 3 => array("x" => $cx + 100, "y" => $cy + 100),
				 4 => array("x" => $cx + 100, "y" => $cy));
}

$objects = array(
	1 => array("condition" => 0, "name" => "Billet d'entrée"),
	2 => array("condition" => 0, "name" => "Plan du parc"),
	3 => array("condition" => 0, "name" => "Pelle"),
	4 => array("condition" => 7, "name" => "Clefs-perdues",
   			   "message_KO" => "Les clefs sont hors de portée ! Un objet long avec une pince au bout fera
   			   					l'affaire."),
	5 => array("condition" => 0, "name" => "Billet perdu"),
	6 => array("condition" => 5, "name" => "Ticket coupe-file",
   			   "message_KO" => "Les privilèges ne sont jamais gratuits. Revenez quand vous aurez fait fortune !"),
	7 => array("condition" => 5, "name" => "Perche à slefis",
   			   "message_KO" => "Cette babiole ne coûte que 42€42 et vous n'avez même pas de quoi la payer.
   			   					Revenez les poches pleines svp !"));

function init_game(){
	$_SESSION["room"] = 1;
	$_SESSION["inventory"] = array();
	$_SESSION["taken_objects"] = array(); //[obj, room, qte ; ...]
}


main();

?>