#Guide du code

##Construction d'un jeu

La fabrication d'un jeu requiert l'écriture d'un fichier JSON cohérent (aucune vérification n'est effectuée à sa lecture). Sa structure est décrite ci-dessous. Il faut également fournir les images mentionnées dans le fichier JSON et les scripts dédiés à chaque page lorsqu'ils existent. La connaissance des identifiants des éléments de base de la page du jeu est nécessaire pour l'écriture des scripts. Je ne les énumère pas ici, vous les trouverez éparpillés dans le code ou plus simplement avec un F12 sur le site.

##Le monde dans un JSON

Un monde est paramétré par un fichier JSON unique qui contient à la fois la description des pièces qui composent le monde ainsi que celle des objets qui parsèment ce monde. Ce fichier JSON doit respecter la structure suivante :

```javascript
    {
        "title": string,
        "subtitle": string,
        "colors": {"background1": color, "background2": color,
                   "border": color, "text": color},
        "images": {"name1": path1, ...}, //Personnages possibles
        "world": {"welcome": string, "good_bye": string,
                  "rooms": [*room*, *room*, ...]},
        "objects": [*object*, *object*, ...]
    }
```

La pièce dont l'id est 1 est impérativement la pièce initiale.


**room** correspond au JSON suivant :

```javascript
    {
        "id": int,
        "name": string,
        "outs": [{"dest": room_id, "by": string, "condition": *obj_set*,
                  "message_OK": string, "message_KO": string},
                  {...}, ...],
        "obj": *obj_set*,
        "color": string, //(couleur CSS)
        "position": [x, y, w, h],
        "end": {"val": "none" | "win" | "loose", "text": string},
        "script": [string, ...]; //Scripts de la pièce
    }
```


**object** correspond au JSON suivant :

```javascript
    {
        "id": int,
        "name": string,
        "condition" : *obj_set*,
        "message_OK" : string,
        "message_KO" : string
    }
```

**obj_set** correspond au JSON suivant :

```javascript
    [
        {"id": obj_id, "quantity": int, "consumed" : bool},
        {...},
        ...
    ]
    //Le champ 'consumed' n'est requis que dans le cas où l'ensemble décrit une condition.
    //Comportement indéfinis en son absence dans ce cas.
```

##L'utilisation des superglobales

###`$_SESSION`

Le jeu ne dispose pas de mécanisme de sauvegarde par base de données. Les données nécessaires à une session de jeu sont stockées dans la variable `$_SESSION`. Les champs utilisés sont répertoriés ci-dessous :
* **`$_SESSION['username']`** = `'myName'`: Pseudonyme du joueur
* **`$_SESSION['image']`** = `'image_URL'`: Image du joueur
* **`$_SESSION['inventory']`** = `[id: number, ...]`: Inventaire du joueur
* **`$_SESSION['position']`** = `room_id`: L'Id de la pièce dans laquelle se trouve le joueur
* **`$_SESSION['taken_obj']`** = `[room: *obj_set*, ...]`: N'ombre de chaque objet pris par pièce

L'utilisation de `$_SESSION` sera la plus limité et élémentatire possible. Chaque fin de script exécutera la conversion des informations à conserver dans `$_SESSION` via les destructeurs et chaque début de script reconstruira les objets dans l'état qui était le leur à la fin du précédent. Au cours du script, on travaille au maximum sur les objets eux_même. On simule ainsi une continuité dans l'exécution du code php tout en limitant l'utilisation de `$_SESSION` au minimum.

Pour permettre la manipulation de variables qui perdurent tout au long du jeu depuis les scripts JS, un mécanisme a été ajouté permettant au designer d'un jeu de stocker des variables dans `$_SESSION` de façon transparente.

###`$_GET` et `$_POST`

`$_POST` n'est utilisée que pour le formulaire initial. Il n'y a pas de raison majeure d'utiliser POST plutôt que GET si ce n'est que POST est un peu plus souple et sécurisé.

`$_GET` est utilisée pour fournir tous les autres paramètres influençant l'exécution du script PHP. Voici les champs utilisés :
* **`$_GET['action']`** = `'action_tag'` : Indique au contrôleur l'action à exécuter. Voici les action possibles :
    - **home** : affichage de la page de connexion du jeu (après choix du jeu)
    - **login** : réception du formulaire de connexion du joueur
    - **logout** : déconnexion du joueur
    - **go** : déplacement du joueur vers une autre salle
    - **take** : ramassage d'un objet
    - **where** : envoie de la position du joueur au format JSON
    - **map** : envoie de la géométrie de la carte au format JSON
    - **set_var** : enregistrment d'une variable depuis un sript JS
    - **read_var** : lecture d'une variable depuis un script JS
* **`$_GET['to']`** = `'room_id'` : Utilisée pour préciser la pièce visée en cas d'action **go**.
* **`$_GET['obj']`** = `'obj_id'` : Utilisé pour préciser l'objet ramassé en cas d'action **take"".
* **`$_GET['name']`** = `'var_name'` : Contient le nom de la variable écrite ou lue depuis un script JS
* **`$_GET['value']`** = `'var_value'` : Contient la valeur de la variable à écrire depuis un script JS

Cette méthode rend le jeu très sensible aux rechargement de page. Si l'on recharge un page après un déplacement réussi, alors le champs `$_GET['to']` ne correspond plus à une sortie de la pièce courrante et une erreur se produit. De même si l'on recharge une page après avoir ramassé le dernier exemplaire d'un objet. Une solution serait d'ajouter une action neutre lancée lorsqu'on remarque qu'on tente de se déplacer vers une pièce qui n'est pas liée à la courrante par une sortie, ou qu'on veut ramasser un objet qui n'est pas présent dans la pièce.


##Architecture générale du site

Les fichiers du projet sont répartis en sous-dossiers : un pour le CSS (fichier de syle unique malgré tout), un pour le JavaScript, un pour le PHP, un pour les images et un pour les fichiers JSON de jeu

La page principale est index.php située à la racine du répertoire du projet. Cette page se contente d'inclure le reste du code et de déclarer les variables globales du jeu. Elle appelle ensuite la méthode `handler()` du game_manager qui lit l'action demandée et appelle la méthode du login_manager, du game_manager ou du game_interface qui convient.


##Les classes (PHP) du jeu

###Le modèle

Le modèle contient toutes les classes représentant des entités passives dans le jeu. Ces entités pourront recevoir des instructions de la part du controleur. En voici la liste :

* **class World** : Cette classe est la réunion de l'ensemble de pièces composant le monde ainsi que de l'ensemble des objets qu'on est susceptibles de rencontrer, le tout dans leur état natif.

* **class Room** : Cette classe représente une pièce du jeu et tous les paramètres qui la caractérisent. Elle peut afficher sur demande la liste de ses sorties, la liste des objets qu'elle offre au joueur ou encore un message de fin de jeu le cas échéant.

* **class Player** : Cette classe représente l'avatar du joueur. Elle fournit essentiellement des informations quand on les lui demandes. Elle peut également afficher la liste des objets dans l'inventaire du joueur.

* **class Inventory** : Cette classe représente un ensemble d'objets en nombre quelconque. Elle peut s'afficher sous forme de liste. Elle permet également de vérifier qu'elle contient les objets requis au sein d'une condition et de répercuter l'application d'une condition (retrait des objets consommés)

    - **class Condition** : Cette classe représente une condition et hérite de la classe Inventory. Elle permet d'effectuer tous les tests et divers opérations utiles sur un iventaire (respect d'une condition, objets manquants, répercution sur l'inventaire, ...)

* **class Object** : Cette classe représente un objet et les informations qui lui sont relatives. Elle ne fait que fournir les informations qui lui sont demandées.


###Le contrôleur

Le contrôleur contient les classes chargées de faire évoluer le jeu. Elles s'occupent de récupérer les actions à effectuer puis les répercutent sur le modèle après avoir réalisé les tests nécessaires. Il finit par appeler la vue pour effectuer l'affichage. Voici les classes qui composent le contrôleur :

* **class Login_manager** : Cette classe gère la récupération du formulaire de connexion et la déconnexion de l'utilisateur.

* **class Game_engine** : Cette classe (dont le nom est bien grand quand on la compare à ce qui se fait de nos jours) est le cœur du contrôleur. Elle répercute les actions de jeu sur le modèle et opère les tests requis.

* **Game_interface** : Cette classe intègre toutes les fonctions de communication vers l'extérieur. En particulier, elle crée les pages JSON demandées par les scripts JS du client et effectue les opérations de lecture et écriture des variables demandées par les scripts JS. On pourrait lui attribuer la gestion de la sauvegarde si elle venait à exister.


###La vue

La vue ne contient qu'une classe (`class Vue_manager`) qui regroupe les différentes méthodes d'affichage. Chade type de page du site (Sélection du monde, formulaire de création d'avatar, en jeu, erreur, déconnexion) possède une méthode appelée par le contrôleur au besoin.


##Remarques générales

Le json est dupliqué pour montrer qu'on a le choix entre les deux versions à la connexion. Créer un monde prend beaucoup de temps, je ne l'ai pas fait deux fois...

Le monde proposé contient des répliques caustiques. A ne pas prendre à titre personnel évidement ! L'intrigue est de plus pourrie (le néant peut-il être pourri d'ailleurs ?). Mais je n'ai pas le temps de faire du Agatha Christie (ni même du Conan Doyle)...

N'étant pas doué en CSS statique et encore moins en responsive design, le site est horriblement rigide. Faites un monde plus grand et vous cassez le design, ajoutez quelques objets et la l'inventaire sort de son div, etc. Mais comme ce n'était pas l'objet du projet je ne me suis pas attardé plus que ça sur le problème.

En revanche, le CSS bénéficie d'une modification dynamique des couleurs uniquement (je ne suis pas téméraire) en PHP.