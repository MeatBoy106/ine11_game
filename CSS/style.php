<?php
	/**
	 * Si le jeu est sélectionné, on utilise un jeu de couleurs personnalisé,
	 * sinon on utilise un jeu par défaut
	 */
	session_start();

	header('content-type: text/css');

	if(isset($_SESSION['game'])){
		$colors = json_decode(file_get_contents("../".$_SESSION['game']), true);
		$colors = $colors['colors'];
		$bg1_color = $colors['background1'];
		$bg2_color = $colors['background2'];
		$border_color = $colors['border'];
		$text_color = $colors['text'];
	} else {
		$bg1_color = "grey";
		$bg2_color = "darkGrey";
		$border_color = "black";
		$text_color = "white";
	}
?>

body {
	width: 907px;
	margin: auto;
}

header {
	background-color: <?php echo $bg1_color; ?>;
	text-align: center;
	border-radius: 10px;
	margin-bottom: 10px;
	margin-top: 5px;
	padding: 5px;
	font-family: arial-black;
	border-style: solid;
	border-color: <?php echo $border_color; ?>;
}

header > h2{
	margin-bottom: 2px;
	color: <?php echo $text_color; ?>;
	font-size: 30px;
}

header > h1 {
	margin-top: 2px;
	color: <?php echo $text_color; ?>;
	font-size: 40px;
}


footer {
	background-color: <?php echo $bg1_color; ?>;
	text-align: center;
	margin-top: 10px;
	border-radius: 10px;
	border-style: solid;
	padding: 5px;
	border-color: <?php echo $border_color; ?>;
	color : <?php echo $text_color; ?>;
	font-weight: 700;
}

#welcome {
	background-color: <?php echo $bg1_color; ?>;
}

#game, #middle {
	border-style: solid;
	border-color: <?php echo $border_color; ?>;
	background-color: <?php echo $bg1_color; ?>;
	border-radius: 10px;
	margin: 0px;
	padding: 0;
	padding-top: 1px;
	font-family: cursive;
}

#middle {
	padding: 10px;
	color : <?php echo $text_color; ?>;
}

#map_canvas {
	display: inline-block;
	vertical-align: top;
	float: left;
	width: 400px;
	height: 300px;
	background-color: <?php echo $bg2_color; ?>;
	border-radius: 5px;
	margin: 10px;
}

#info {
	display: inline-block;
	vertical-align: top;
	float: right;
	width: 450px;
	height: 290px;
	background-color: <?php echo $bg2_color; ?>;
	border-radius: 5px;
	margin: 10px;
	padding: 5px;
}

#player_name {
	margin-top: 3px;
	margin-bottom: 3px;
	text-align: center;
	color: <?php echo $text_color; ?>;
	font-size: 25px;
}

#position {
	color: <?php echo $text_color; ?>;
	text-align: center;
	font-size: 20px;
}

#inv {
	color: <?php echo $text_color; ?>;
}

#story {
	display: inline-block;
	vertical-align: top;
	width: 855px;
	margin: 10px;
	background-color: <?php echo $bg2_color; ?>;
	border-radius: 5px;
	padding: 8px;
	padding-left: 12px;
	padding-right: 12px;
	color: <?php echo $text_color; ?>;
}

#lacking_obj > p {
	font-size: 20px;
}

#end_text {
	display: none;
}

#moves {
	display: inline-block;
	vertical-align: top;
	background-color: <?php echo $bg2_color; ?>;
	width: 500px;
	height: 170px;
	margin: 10px;
	padding: 5px;
	border-radius: 5px;
	color: <?php echo $text_color; ?>;
}

#loot {
	display: inline-block;
	vertical-align: top;
	background-color: <?php echo $bg2_color; ?>;
	width: 340px;
	height: 170px;
	margin: 10px;
	padding: 5px;
	border-radius: 5px;
	color: <?php echo $text_color; ?>;
}

#moves a, #loot a {
	color: <?php echo $text_color; ?>;
}

#moves p, #loot p {
	color: <?php echo $text_color; ?>;
	font-size: 20px;
}

#logout {
	text-align: center;
	margin-bottom: 10px;
	padding-bottom: 10px;
	margin-top: 5px;
}

#logout > a {
	text-decoration: none;
	color: red;
	font-size: 20px;
	font-family: impact;
	font-weight: 100;
	border-style: solid;
	border-radius: 5px;
	padding: 5px;
}

#logout > a:hover {
	text-decoration: none;
	font-size: 25px;
}

input[type=text], select {
    width: 200px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    width: 100%;
    background-color: #177DC1;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #2E62D8;
}

form {
    width: 50%;
    margin: 0 auto;
}

label, input {
    display: inline-block;
}

label {
    width: 30%;
    text-align: right;
}

label + input {
    width: 30%;
    margin: 0 30% 0 4%;
}

input + input {
    float: right;
}
​