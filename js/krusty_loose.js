/*
 * Ce script décompte les passages sur la salle de défaite (Broyeur à ordures)
 * et déclare la défaite au 3e passage.
 */

passing = read_in_session('passing');

if(window.location.search.indexOf('action=go') !== -1){
	passing ++;
	console.log(passing);
	write_in_session('passing', passing);
}

if(passing >= 3){
	var end_message = document.getElementById('end_text');
	end_message.style.display = 'block';

	var moves = document.getElementById('moves');
	moves.style.display = 'none';

	var loot = document.getElementById('loot');
	loot.style.display = 'none';
}