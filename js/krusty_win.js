/**
 * Ce script retire les div de déplacement et ramassage et affiche le message
 * de victoire à l'entrée sur la salle de victoire (Parking)
 */

	var end_message = document.getElementById('end_text');
	end_message.style.display = 'block';

	var moves = document.getElementById('moves');
	moves.style.display = 'none';

	var loot = document.getElementById('loot');
	loot.style.display = 'none';