//Ecrire une variable nommée "var_name" et valant "val" dans la session
function write_in_session(var_name, val) {
	//Une requête passant les paramètres en GET au PHP
    $.ajax({
        url: 'index.php',
        type: 'GET',
        data: 'action=set_var&value=' + val + '&name='+ var_name,
    });
}


//Récupérer la valeur de la variable "var_name" stockée dans la session
function read_in_session(var_name) {
	//Requête asynchrone pour effectuer le return une fois la variable définie
    $.ajax({
        url: 'index.php',
        type: 'GET',
        data: 'action=read_var&name=' + var_name,
        dataType : 'json',
        async: false,
        success: function(data, textStatus, xhr) {
        	//Conversion en Int
            value = parseInt(data);
        }
    });

    return value;
}