$(document).ready(function(){
	//Requête synchrone pour contrôler l'ordre d'affichage (carte puis avatar)
	$.ajax({
		url: 'index.php?',
		type: 'GET',
		data: 'action=map',
		dataType: 'json',
		async: 'false',
		success: function(map) {
			var canvas = document.getElementById('map_canvas');
			var ctx = canvas.getContext('2d');

			$.each(map, function(index, room){
				//Coloriage, coloriage, coloriage
				ctx.fillStyle = room.color;
				ctx.lineWidth = 3;
				ctx.fillRect(room.geometry.x, room.geometry.y, room.geometry.w, room.geometry.h);
				ctx.strokeRect(room.geometry.x, room.geometry.y, room.geometry.w, room.geometry.h);
			})
		}
	});

	//Requête classique ensuite
	$.getJSON("index.php?action=where", function(data) {
		var canvas = document.getElementById('map_canvas');
		var ctx = canvas.getContext('2d');
		var img = new Image();

		//Une fois l'image chargée...
		img.onload = function(){
			//la dessiner au milieu !
			ctx.drawImage(img,
						  data.position[0] - img.width / 2,
						  data.position[1] - img.height / 2,
						  img.width, img.height);
		};
		img.src = data.image;
	});
});